const puppeteer = require('puppeteer');
const $ = require('cheerio')

require('dotenv').config();

const nodemailer = require('nodemailer')

var counter = 0;
// var product = {};
var done = false;


let sendMail =async (oldprice,data)=>{
    let transporter = nodemailer.createTransport({
        host: process.env.GMAIL_HOST,
        port: process.env.GMAIL_PORT,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.GMAIL_USERNAME, // generated ethereal user
          pass: process.env.GMAIL_PASSWORD, // generated ethereal password
        },
      });
    
      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: '"Kiwi Poon" <letskatehk@gmail.com>', // sender address
        to: "alllovemewai@outlook.com,akirahei@gmail.com", // list of receivers
        subject: `[偉偉的價錢追蹤] 你關注的物品價格下降到 ${data.price} 了 - ${data.productName}`, // Subject line
        // text: "Hello world?", // plain text body
        html: `<div><h3>你關注的物品價格下降了</h3><div>物品名稱 : ${data.productName}</div><br /><div>價錢由 ￥${oldprice} 減至 <b>￥${data.price}</b></div><br /><div>物件傳送門: <a href="${data.url}">按我</a></div><br /><div><img src="${data.img}" alt="物件圖片" /></div></div>`, // html body
      });

      console.log('Email Send');
}

let scrape = async () => {
    const url = "https://www.amazon.co.jp/%E3%82%B7%E3%83%A5%E3%83%BC%E3%83%97%E3%83%AA%E3%83%BC%E3%83%A0-%E3%83%AD%E3%83%B3%E3%82%B0%E3%83%AF%E3%83%B3%E3%83%94%E3%83%BC%E3%82%B9-%E3%82%B9%E3%82%BF%E3%83%B3%E3%83%89%E3%82%AB%E3%83%A9%E3%83%BC%E3%83%8E%E3%83%BC%E3%82%B9%E3%83%AA%E3%83%BC%E3%83%96%E3%83%AF%E3%83%B3%E3%83%94%E3%83%BC%E3%82%B9-%E3%83%AC%E3%83%87%E3%82%A3%E3%83%BC%E3%82%B9-%E6%97%A5%E6%9C%AC%E3%82%B5%E3%82%A4%E3%82%BAS-M%E7%9B%B8%E5%BD%93/dp/B086K6G4YZ/ref=redir_mobile_desktop/355-2292659-6928368?ie=UTF8&psc=1&ref_=ox_sc_saved_image_10&smid=AN1VRQENFRJN5";
    // const PriceSelector = "";
    // const productTitleSelector = "";

    const browser = await puppeteer.launch({ executablePath: '/usr/bin/chromium-browser' })
    const page = await browser.newPage();

    await page.goto(url, { waitUntil: "networkidle2" });
    var oldPrice = 0;
    

    while (!done) {
        var product;
        if (await page.$("#priceblock_ourprice") !== null) {
            // console.log('Selector #priceblock_ourprice found');
            product = await page.evaluate(() => {
                let productName = document.querySelector("#productTitle").innerHTML.trim();
                let price = parseInt(document.querySelector("#priceblock_ourprice").innerHTML.trim().replace('￥', '').replace(',', ''));
                let url = document.location.href;
                let img = document.querySelector("#landingImage").src;

                // console.log();
                return {
                    productName,
                    price,
                    url,
                    img
                }
            })
            // let now = new Date();

            console.log(`${counter} times attempted at ${new Date(Date.now()).toLocaleString('en-US',{timeZone:'Asia/Taipei'})}`);
            // console.log(product);
            if(oldPrice==0){
                oldPrice = product.price;
            }else if(product.price < oldPrice){
                console.log(`The price drop from ${oldPrice} to ${product.price}`);
                await sendMail(oldPrice,product);
                oldPrice = product.price;
            }else if(product.price == oldPrice){
                console.log(`The price remained unchanged`);
            }else if(product.price > oldPrice){
                console.log(`The price raise from ${oldPrice} to ${product.price}`);
            }

            counter++;
            await page.waitFor(60000);
        } else {
            console.log('Selector not found');
            done = !done;
        }
    }
    browser.close();
}


scrape();

